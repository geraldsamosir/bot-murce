'use strict'

const WaBot = require('./src/transport/whatsapp-js')

const transports = process.argv.slice(2)

transports.forEach(transport => {
   switch (transport) {
     case 'wa-bot':
       const waBot = new WaBot()
       return waBot.run()
     default:
       console.log('unkown transports', transport)
   }
})
