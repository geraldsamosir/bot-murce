'use strict'

const { Client } = require('whatsapp-web.js')
const qrcode = require('qrcode-terminal')
const whatsappJSRoutes = require('./routes')

class whatsappJS {

  constructor () {
    this.Client = new Client()   
  }

  run () {
    this.Client.on('qr', (qr) => {
       qrcode.generate(qr, { small: true }) 
    })

    this.Client.on('ready', () => {
       console.info('Bot Ready :) ...')
    })

    this.Client.on('message', async (msg) => {
      await whatsappJSRoutes(msg.body, msg) 
    })

    this.Client.initialize()
  }
}


module.exports = whatsappJS