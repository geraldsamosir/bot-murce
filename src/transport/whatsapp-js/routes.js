'use strict'

const { MessageMedia } = require('whatsapp-web.js');

const { 
  JokesServices,
  PokemonService
}  = require('../../services/')

const whatsappJSRoutes = async(routes, msg) => {
  switch (true) {
    case /^!ping/.test(routes): 
      msg.reply('pong')
      break
    case /jokes/.test(routes): 
      const joke = await JokesServices.getJoke()
      msg.reply(joke)
      break;
    case /^pokemon/.test(routes):
      const [_, pokemonName ] = routes.split(' ')
      const pokemonImageUrl = await PokemonService.getPokemonImageUrl(pokemonName)
  
      if(!pokemonImageUrl) return msg.reply('pokemon yang kamu cari tidak ada :(')
  
      const media = await MessageMedia.fromUrl(pokemonImageUrl)
      msg.reply(media)
      break
    default:
      console.log('unkown')
      return
  }
} 

module.exports = whatsappJSRoutes
