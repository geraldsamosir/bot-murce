'use strict'

const JokesServices = require('../')

describe('JokesServices.getJoke', () => {
    test('should be success', async () => {
      jest.spyOn(JokesServices.httpClient, 'get').mockResolvedValueOnce({
        data: {
           joke: 'joke' 
        }
      })

      const resultJokes = await JokesServices.getJoke()
      expect(resultJokes).toEqual('joke')
    })
})