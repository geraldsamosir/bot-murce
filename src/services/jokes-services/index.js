'use strict'

const axios = require('axios')

const { 
  httpHeaderDefaultConfig,
  serviceBaseUrl: {
    jokes: jokeBaseUrl
  },
  httpDefaultTimeOut
} = require('../../config/constant')

class JokesServices {
   constructor () {
      this.httpClient = axios.create({
        baseURL: jokeBaseUrl,
        headers: {
         ...httpHeaderDefaultConfig
        }
      })

      this.httpClient.defaults.timeout = httpDefaultTimeOut;
   }

   /**
    * Get random Jokes
    *
    * @returns  {Promise<string>}
    */
   async getJoke () {
     const resultHttp = await this.httpClient.get('/')
     return resultHttp?.data?.joke
   }
}

module.exports = new JokesServices()

