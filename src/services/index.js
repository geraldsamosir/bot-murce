'use strict'

const  JokesServices = require('./jokes-services')
const PokemonService = require('./pokemon-services')

module.exports = {
  JokesServices,
  PokemonService
}