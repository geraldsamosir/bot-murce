'use strict'

const PokemonService = require('../')

const getPokemeonImageMock = jest.spyOn(PokemonService.httpClient, 'get')

describe('PokemonService.getPokemonImageUrl', () => {
    test('should be success', async () => {
      getPokemeonImageMock.mockResolvedValueOnce({
        data: {
          sprites: {
            other: {
              dream_world: {
                front_default: 'pikachu_url'
              }
            }
          }
        }
      })

      const pokemImageUrl = await PokemonService.getPokemonImageUrl('pikachu')
    
      expect(pokemImageUrl).toEqual('pikachu_url')
    })

    test('should be success - image url not found',  async () => {
      getPokemeonImageMock.mockResolvedValueOnce()

      const pokemImageUrl = await PokemonService.getPokemonImageUrl('pikachu')

      expect(pokemImageUrl).toEqual()

    })
})