'use strict'

const axios = require('axios')

const { 
  httpHeaderDefaultConfig,
  serviceBaseUrl: {
    pokemon: pokemonBaseUrl
  },
  httpDefaultTimeOut
} = require('../../config/constant')

class PokemonService {
  constructor () {
    this.httpClient = axios.create({
        baseURL: pokemonBaseUrl,
        headers: {
         ...httpHeaderDefaultConfig
        }
    })

    this.httpClient.defaults.timeout = httpDefaultTimeOut
  }

  /**
   * Get pokemon image url by pokemon name
   * 
   * @param {string} pokemonName 
   * 
   * @returns {Promise<string>} 
   */
  async getPokemonImageUrl (pokemonName) {
     const resultHttp = await this.httpClient.get(`/${pokemonName}`) 

     if (!resultHttp?.data) return

     const {
        sprites: {
          other: { 
            dream_world: { 
              front_default: defaultImageUrl
            }
          }
        }
     } = resultHttp.data

     return defaultImageUrl
  }
}

module.exports = new PokemonService()
