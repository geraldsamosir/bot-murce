module.exports = {
   httpHeaderDefaultConfig: {
    'Content-Type': 'application/json',
     'Accept': 'application/json'
   },
   serviceBaseUrl: {
      pokemon: 'https://pokeapi.co/api/v2/pokemon',
      jokes: 'https://icanhazdadjoke.com'
   },
   httpDefaultTimeOut: 2500
}