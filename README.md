# 🤖 Bot murce $$$

# Flow process handler
```mermaid
sequenceDiagram
    participant us as users
    participant ch as chat app (WA)
    participant bc as Bot Channel
    participant sv as service handler
    participant tp as third party /db / etc
    us ->> ch: send message
    ch ->> bc: triger incomming message
    bc->>sv: coming request with code tag
    sv ->> tp: request data your need it
    tp ->> sv: data given
    sv ->> bc: send the meta data
    bc ->> ch: send message
    ch ->> us: show message response
```
demo -> [demo/vidma_recorder_23102023_155527.mp4](https://gitlab.com/geraldsamosir/bot-murce/-/blob/main/demo/vidma_recorder_23102023_155527.mp4?ref_type=heads)

related libary you need and maybe you can improve to make this app more better
- https://wwebjs.dev/
- https://core.telegram.org/bots/tutorial (if you want to add channel just continue in transports folder)


node version ^(v14.0.0) 
